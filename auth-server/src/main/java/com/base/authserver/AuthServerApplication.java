package com.base.authserver;

import com.base.authserver.model.Role;
import com.base.authserver.model.User;
import com.base.authserver.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class AuthServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner run(UserService userService){
        return args -> {
          userService.saveRole(new Role(null,"ROLE_USER"));
          userService.saveRole(new Role(null,"ROLE_MANAGER"));
          userService.saveRole(new Role(null,"ROLE_ADMIN"));
          userService.saveRole(new Role(null,"ROLE_SUPER_ADMIN"));

          userService.saveUser(new User(null,"cihan","chn","1234",new ArrayList<>()));

          userService.addRoleToUser("chn","ROLE_USER");
          userService.addRoleToUser("chn","ROLE_MANAGER");
          userService.addRoleToUser("chn","ROLE_ADMIN");
          userService.addRoleToUser("chn","ROLE_SUPER_ADMIN");
        };
    }
}
