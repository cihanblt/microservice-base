package com.base.authserver.service;

import com.base.authserver.model.Role;
import com.base.authserver.model.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);

    Role saveRole(Role role);

    void addRoleToUser(String userName, String roleName);

    User getUser(String userName);

    List<User> getUsers();


}
