package com.base.streamconsumerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamConsumerServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(StreamConsumerServiceApplication.class, args);
    }
}
