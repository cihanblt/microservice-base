package com.base.streamproducerservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class StreamProducerServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(StreamProducerServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(KafkaTemplate<String,String> kafkaTemplate){
        return args -> {
          kafkaTemplate.send("my-new-topic","hello kafka");
        };
    }
}
